import React from "react";
import ClientRow from "./ClientRow";

const Tableau = ({ clients }) => {
  const rows = [];
  for (const client of clients) {
  
    rows.push(<ClientRow key={client._id} client={client} />);
  }
  return (
    <table className="table">
      <thead className="table-light">
        <tr>
          <th scope="col">Nom</th>
          <th scope="col">Prenom</th>
          <th scope="col">Numero</th>
          <th scope="col">Email</th>
        </tr>
      </thead>
      <tbody>{rows}</tbody>
    </table>
  );
};

export default Tableau;
