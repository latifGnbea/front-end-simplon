import React from "react";

const ClientRow = ({ client }) => {
  return (
    <tr>
      <td> {client.nom}</td>
      <td> {client.prenom}</td>
      <td> {client.numero}</td>
      <td> {client.email}</td>
    </tr>
  );
};

export default ClientRow;
