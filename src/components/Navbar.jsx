// Navbar.js
import React from "react";
import { Link, useLocation } from "react-router-dom";
import logo from "../assets/images/logo.png"

const Navbar = () => {
  const location = useLocation();

  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light px-3">
      <Link className="navbar-brand" to="/">
        <img src={logo} className="me-2" alt="Logo" height="30" />
        Latif entreprise
      </Link>
      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarNav"
        aria-controls="navbarNav"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>
      <div
        className="collapse navbar-collapse justify-content-end"
        id="navbarNav"
      >
        <ul className="navbar-nav">
          <li className="nav-item ">
            <Link
              className={`nav-link ${
                location.pathname === "/" ? "active" : ""
              }`}
              to="/"
            >
              Accueil
            </Link>
          </li>
          <li className="nav-item ">
            <Link
              className={`nav-link ${
                location.pathname === "/add-client" ? "active" : ""
              }`}
              to="/add-client"
            >
              Ajouter un client
            </Link>
          </li>
        </ul>
      </div>
    </nav>
  );
};

export default Navbar;
