import { Route, Routes } from "react-router-dom";
import "./App.css";
import Home from "./pages/Home";
import AddClient from "./pages/AddClient";
import Layout from "./components/Layout";
import Error from "./pages/Error";

function App() {
  return (
    <Routes>

      <Route path="/" element={<Layout />}>
        <Route index element={<Home />} />
        <Route path="/add-client" element={<AddClient />} />
      </Route>
      
      <Route path="*" element={<Error />} />
    </Routes>
  );
}

export default App;
