// store.js
import { configureStore } from "@reduxjs/toolkit";
import clientReducer from "./client/clientSlice";

const store = configureStore({
  reducer: {
    client: clientReducer,
  },
});

export default store;
