import { createAction, createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import clientService from "./clientService";

// Toutes les catégories
export const allClient = createAsyncThunk(
  "client/allClient",
  async (_, { rejectWithValue }) => {
    try {
      const response = await clientService.getAllClient();
      // Assurez-vous que les données de réponse sont valides avant de les renvoyer
      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide");
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

export const createdClient = createAsyncThunk(
  "client/create-new-client",
  async (newClient, { rejectWithValue }) => {
    try {
      const response = await clientService.createClient(newClient);
      

      if (response && response.data) {
        return response.data;
      } else {
        return rejectWithValue("Réponse de l'API invalide", +response);
      }
    } catch (error) {
      return rejectWithValue(error.message);
    }
  }
);

// Réinitialiser le state
export const resetClientState = createAction("client/resetState");

const initialState = {
  clients: [],
  isError: false,
  isSuccess: false,
  isLoading: false,
  errorMessage: "",
};

export const clientSlice = createSlice({
  name: "clients",
  initialState,
  reducers: {},
  extraReducers: (builder) => {
    builder
      .addCase(allClient.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(allClient.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        state.clients = action.payload;
        state.errorMessage = "";
      })
      .addCase(allClient.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(createdClient.pending, (state) => {
        state.isLoading = true;
      })
      .addCase(createdClient.fulfilled, (state, action) => {
        state.isLoading = false;
        state.isSuccess = true;
        state.isError = false;
        state.clients.push(action.payload); 
        state.errorMessage = "";
      })
      .addCase(createdClient.rejected, (state, action) => {
        state.isLoading = false;
        state.isSuccess = false;
        state.isError = true;
        state.errorMessage = action.payload;
      })
      .addCase(resetClientState, (state) => {
        return initialState;
      });
  },
});

export default clientSlice.reducer;
