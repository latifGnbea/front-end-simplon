import axios from "axios";
import { base_url } from "../../utils/baseUrl";

export const getAllClient = async () => {
  try {
    const response = await axios.get(`${base_url}client/all`);
    // Vérifiez si la réponse a un code de statut HTTP 200 (OK)
    if (response.status === 200) {
      return response; // Les données de la catégorie sont renvoyées
    } else {
      throw new Error(
        "La requête a échoué avec un code de statut: " + response.status
      );
    }
  } catch (error) {
    // Renvoie une erreur avec un message descriptif
    throw new Error(
      "Une erreur s'est produite lors de la récupération des clients: " +
        error.message
    );
  }
};

// creer
const createClient = async (client) => {
  try {
    const reponse = await axios.post(`${base_url}client/create`, client);
    return reponse;
  } catch (error) {
    console.log(error);
   
  }
};


const clientService = {
  getAllClient,
  createClient
};

export default clientService;
