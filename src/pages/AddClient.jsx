import React, { useEffect } from "react";
import { useFormik } from "formik";
import * as Yup from "yup";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { createdClient, resetClientState } from "../redux/client/clientSlice";
import { toast } from "react-toastify";

const ClientSchema = Yup.object().shape({
  nom: Yup.string().required("Champ requis"),
  prenom: Yup.string().required("Champ requis"),
  numero: Yup.string().required("Champ requis"),
  email: Yup.string().required("Champ requis"),
});
const AddClient = () => {
  const navigate = useNavigate();
  const dispatch = useDispatch();


  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      nom:  "",
      prenom:  "",
      numero:  "",
      email:  "",
    },
    validationSchema: ClientSchema,
    onSubmit: async (values) => {
      alert(JSON.stringify(values))
      const response = await dispatch(createdClient(values));

      if (response.type == "client/create-new-client/fulfilled") {
        toast.success("Nouvelle ajoute avec sucess");
        formik.resetForm();
        dispatch(resetClientState());
        navigate("/");

      } else {
        console.log(response.response);
        toast.error("Echec lors de l'ajout");
      }
    },
  });
  return (
    <div>
      <h4> Ajouter un client</h4>
      <form
        action=""
        onSubmit={formik.handleSubmit}
        className="container-fluid mx-0 "
      >
        <div className="mt-5">
          <input
            type="text"
            className="form-control form-control-lg"
            placeholder="Nom"
            name="nom"
            onChange={formik.handleChange}
            value={formik.values.nom}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger text-start">
            {formik.touched.nom && formik.errors.nom ? (
              <div>{formik.errors.nom}</div>
            ) : null}
          </div>
        </div>
        <div className="mt-3">
          <input
            type="text"
            className="form-control form-control-lg"
            placeholder="Prenom"
            name="prenom"
            onChange={formik.handleChange}
            value={formik.values.prenom}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger text-start">
            {formik.touched.prenom && formik.errors.prenom ? (
              <div>{formik.errors.prenom}</div>
            ) : null}
          </div>
        </div>

        <div className="mt-3">
          <input
            type="text"
            className="form-control form-control-lg"
            placeholder="Numero de telephone"
            name="numero"
            onChange={formik.handleChange}
            value={formik.values.numero}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger text-start">
            {formik.touched.numero && formik.errors.numero ? (
              <div>{formik.errors.numero}</div>
            ) : null}
          </div>
        </div>

        <div className="mt-3">
          <input
            type="email"
            className="form-control form-control-lg"
            placeholder="Email"
            name="email"
            onChange={formik.handleChange}
            value={formik.values.email}
            onBlur={formik.handleBlur}
          />
          <div className="text-danger text-start">
            {formik.touched.email && formik.errors.email ? (
              <div>{formik.errors.email}</div>
            ) : null}
          </div>
        </div>
        <button className="btn btn-outline-success btn-lg mt-2 float-end px-5">
          Ajouter
        </button>
      </form>
    </div>
  );
};

export default AddClient;
