import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { allClient } from "../redux/client/clientSlice";
import Tableau from "../components/table/Tableau";

const Home = () => {
    const dispatch = useDispatch()

    useEffect(() => {
     dispatch(allClient())
    }, [])
    const dataClient = useSelector((state) => state.client.clients);
    console.log(dataClient);
    
  return (
    <div>
      <h1 className="text-start mb-3 text-decoration-underline text-secondary">
        Liste des clients
      </h1>
      <Tableau clients = {dataClient} />
    </div>
  );
};

export default Home;
